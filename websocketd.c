#ifdef __cplusplus
extern "C" {
#endif

#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "websocketd.h"

int main( int argc, char *argv[] ) {

  // Reserve variables
  char   *aStatic;   // Static directory string
  char   *aDir;      // WS directory string
  char   *aPort;     // Listen port string version
  int     iPort;     // Listen port integer
  int     listen_sd; // Listening socket descriptor
  int     client_sd; // Newly-accepted socket descriptor
  fd_set  readfds;   // List of descriptors to check
  int     opt = 1;
  struct sockaddr_in addr;
  int   addrlen = sizeof(addr);

  // Fetch initial from env
  aStatic = getenv("STATIC");
  aDir    = 0;
  aPort   = getenv("PORT");

  // Detect listening port
  iPort = WSD_PORT;
  if (aPort) iPort = atoi(aPort);

  // TODO: Parse arguments

  // Create sockets
  listen_sd = socket(AF_INET, SOCK_STREAM, 0);
  if (listen_sd < 0) {
    perror("socket() failed");
    exit(1);
  }

  // Make the listening socket non-blocking
  if ( setsockopt(listen_sd, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ) {
    perror("setsockopt() failed");
    exit(2);
  }

  // Setup what to listen for
  addr.sin_family      = AF_INET;
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_port        = htons( iPort );

  // Bind the socket
  if (bind(listen_sd, (struct sockaddr *)&addr, addrlen) < 0) {
    perror("bind() failed");
    exit(3);
  }

  // Start listening, max 5 pending
  if (listen(listen_sd, 5) < 0) {
    perror("listen() failed");
    exit(4);
  }

  // Accept incoming connection
  printf("Listening on port: %d\n", iPort);
  while(1) {

    // Clear our checklist
    FD_ZERO(&readfds);

    // Add the listening socket to the checklist
    FD_SET(listen_sd, &readfds);

    // Add clients to the checklist
    // TODO: CLIENT LIST
    break;
  }

  return 42;
}

#ifdef __cplusplus
} // extern "C"
#endif
