#ifndef _WEBSOCKET_DAEMON_H_
#define _WEBSOCKET_DAEMON_H_

#ifdef __cplusplus
extern "C" {
#endif

#define WSD_PORT 8080

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _WEBSOCKET_DAEMON_H_
