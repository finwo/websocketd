SRC = websocketd.c
OBJ = $(SRC:.c=.o)

all: options websocketd

options:
	@echo websocket build options:
	@echo "CFLAGS  = $(CFLAGS)"
	@echo "LDFLAGS = $(LDFLAGS)"
	@echo "CC      = $(CC)"

config.h:
	cp config.def.h config.h

.c.o:
	$(CC) $(CFLAGS) -c $<

websocketd.o: config.h websocketd.h

$(OBJ): config.h

websocketd: $(OBJ)
	$(CC) -o $@ $(OBJ) $(LDFLAGS)

clean:
	rm -f websocketd $(OBJ)

install: websocketd
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f websocketd $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/websocketd
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp websocketd.1 $(DESTDIR)$(MANPREFIX)/man1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/websocketd.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/websocketd
	rm -f $(DESTDIR)$(MANPREFIX)/man1/websocketd.1

.PHONY: all options clean install uninstall
